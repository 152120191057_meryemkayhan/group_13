#include <iostream>
#include <vector>
#include "Simulation.h"
#include "Engine.h"
#include "FuelTank.h"
#include <string>
#include <time.h>
#include <sstream>
using namespace std;
Simulation::Simulation()
{
	this->Time = 0;
	this->fuel_tank_count = 0;
}
string Simulation::connect_fuel_tank_to_engine(int id)
{
	bool flag = false;
	for (auto it = tanks.begin(); it != tanks.end(); ++it)
	{
		int idtemp = (*it).getfuel_tank_id();
		if (idtemp == id)
		{
			(*it).setconnection(true);
			flag = true;
			ConnectedTanks.push_back((*it));
		}
	}
	if (flag == false)
		return "No fuel tank found at the specified id";
	else
		return "Fuel tank connected to the engine";
}
string Simulation::disconnect_fuel_tank_from_engine(int id)
{
	bool flag = false;
	vector<FuelTank>::iterator iters;
	for (auto it = tanks.begin(); it != tanks.end(); ++it)
	{
		int idtemp = (*it).getfuel_tank_id();
		if (idtemp == id)
		{
			(*it).setconnection(false);
			(*it).setValveStatus(false);
			flag = true;
			iters = it;
		}
	}
	if (flag == false)
		return "No fuel tank found at the specified id";
	else
	{
		for (auto iter = ConnectedTanks.begin(); iter != ConnectedTanks.end(); ++iter)
		{
			if ((*iter).getfuel_tank_id() == (*iters).getfuel_tank_id())
			{
				ConnectedTanks.erase(iter);
				break;
			}
		}
		for (auto iter = ValveOpenTanks.begin(); iter != ValveOpenTanks.end(); ++iter)
		{
			if ((*iter).getfuel_tank_id() == (*iters).getfuel_tank_id())
			{
				ValveOpenTanks.erase(iter);
				break;
			}
		}
		return "Fuel tank disconnected from the engine";
	}
}
vector<string> Simulation::list_connected_tanks()
{
	vector<string> list_connected_tanks;
	for (auto i = tanks.begin(); i != tanks.end(); ++i)
	{
		if ((*i).getconnection() == true)
		{
			string temp = "";
			temp = "tank id: " + to_string((*i).getfuel_tank_id()) + " capacity: " + to_string((*i).getCapacity()) + " fuel quantity: " + to_string((*i).getFuelQuantity());
			list_connected_tanks.push_back(temp);
		}
	}
	if (list_connected_tanks.size() == 0) cout << "There is no connected tank." << endl;
	else return list_connected_tanks;

}
string Simulation::fuelConsumption(double t)
{
	if (t <= 0) return "Invalid wait command input. Time must be greater than 1.";
	else if (engine->getStatus() == false) return "The engine is stopped right now.";
	for (int i = 0; i < t && AbsorbControl() == true; i++)
	{
		double miktar = engine->getInternalTankQuantity();
		engine->setInternalTankQuantity(miktar - 5.5);
		set_time(1);
	}
	if (AbsorbControl() == false) return "There is no connected tank to absorb fuel enough.";
	return "Consumption is occured";
}
string Simulation::stop_engine()
{
	if (engine->getStatus() == true)
	{
		///give the fuel in the internal tank back
		give_back_fuel(engine->getInternalTankQuantity());
		for (auto it = tanks.begin(); it != tanks.end(); ++it)
		{
			(*it).setValveStatus(false);
		}
		engine->setStatus(false);
		return "Engine stopped!";
	}
	else
		return "Engine has already stopped!";
}
string Simulation::open_valve(int id)
{
	if (tanks.empty())	return "There is no tank to open valve";

	for (auto it = tanks.begin(); it != tanks.end(); ++it)
	{
		int idtemp = (*it).getfuel_tank_id();
		if (idtemp == id)
		{
			///check if the tank is not broken
			if ((*it).getBrokenStatus() == false)
			{
				///check if the tank is connected
				if ((*it).getconnection() == true)
				{
					///check if the valve is closed
					if ((*it).getValveStatus() == false)
					{
						(*it).setValveStatus(true);
						ValveOpenTanks.push_back(*it);
						AbsorbControl();
						return "The valve is open!";
					}
					else
						return "The valve is already open!";
				}
				else
					return "The tank is not connected!";
			}
			else
				return "The tank is broken!";
		}
	}
	return "There is no such a tank with " + to_string(id) + " to open its valve";
}
bool Simulation::AbsorbControl()
{
	if (engine->getInternalTankQuantity() > 20)	return true;
	if (ValveOpenTanks.size() < 1)	return false;
	srand(time(NULL));

	bool flag = true;
	vector<FuelTank>::iterator it;
	while (flag == true)
	{
		double V = -1;

		int piyango;
		do
		{
			int _piyango = rand() % ValveOpenTanks.size();

			for (auto iter = tanks.begin(); iter != tanks.end(); ++iter)
			{
				if (ValveOpenTanks[_piyango].getfuel_tank_id() == (*iter).getfuel_tank_id())
				{
					V = (*iter).getFuelQuantity();
					it = iter;
					break;
				}
			}
			piyango = _piyango;
		} while (V <= 0);
		double absorbQuantity = engine->get_internal_tank_capacity() - engine->getInternalTankQuantity();
		engine->setInternalTankQuantity((V >= absorbQuantity ? 55 : (engine->getInternalTankQuantity() + V)));

		double fuelQ = (V >= absorbQuantity ? V - absorbQuantity : 0);
		ValveOpenTanks[piyango].setFuelQuantity(fuelQ);
		(*it).setFuelQuantity(fuelQ);

		flag = false;
	}
	return true;
}

Simulation::~Simulation()
{
	tanks.clear();
}
string Simulation::add_fuel_tank(int capacity)
{
	this->fuel_tank_count++;
	tanks.push_back(FuelTank(capacity, fuel_tank_count));
	string temp = "";
	temp = "Fuel tank " + to_string(this->fuel_tank_count) + " added ";
	return temp;
}
vector<string> Simulation::list_fuel_tanks() const
{
	if (tanks.empty())
	{
		vector<string> liste;
		liste.push_back("There is no external tank");
		return liste;
	}

	vector<string> liste;
	for (auto it = tanks.begin(); it != tanks.end(); ++it)
	{
		string list = "";
		list = "tank_id: " + to_string((*it).getfuel_tank_id()) + " capacity: " + to_string((*it).getCapacity()) + " fuel_quantity: " + to_string((*it).getFuelQuantity());
		liste.push_back(list);
	}
	return liste;
}
string Simulation::print_fuel_tank_count() const
{
	string count = "";
	count = "fuel_tank_count: " + to_string(this->fuel_tank_count);
	return count;
}
string Simulation::remove_fuel_tank(int id)
{
	if (this->tanks.empty())
	{
		return "There is no external tank";
	}
	vector<FuelTank>::iterator its;
	for (auto it = tanks.begin(); it != tanks.end(); ++it)
	{
		int idtemp = (*it).getfuel_tank_id();
		if (idtemp == id)
		{
			its = it;
		}
	}
	for (auto iter = ConnectedTanks.begin(); iter != ConnectedTanks.end(); ++iter)
	{
		if ((*its).getfuel_tank_id() == (*iter).getfuel_tank_id())
		{
			ConnectedTanks.erase(iter);
			break;
		}
	}
	tanks.erase(its);
	return "Fuel tank removed";
}
double Simulation::print_total_fuel_quantity()
{
	double sum = 0;
	for (auto i = tanks.begin(); i != tanks.end(); ++i)
	{
		if ((*i).getconnection() == true)
			sum += (*i).getFuelQuantity();
	}
	sum += engine->getInternalTankQuantity();
	return sum;
}
void Simulation::set_time(double t) { this->Time += t; }
double Simulation::print_consumed_fuel_quantity() { return engine->getFuelPerSecond()*Time; }
string Simulation::print_tank_info(int id)
{
	for (auto i = tanks.begin(); i != tanks.end(); ++i)
	{
		if ((*i).getfuel_tank_id() == id)
		{
			return ("tank id: " + to_string(tanks[id-1].getfuel_tank_id()) +
				" tank capacity: " + to_string(tanks[id-1].getCapacity()) +
				" fuel quantity: " + to_string(tanks[id-1].getFuelQuantity()) +
				" valve situation: " + to_string(tanks[id-1].getValveStatus()) +
				" connection situation: " + to_string(tanks[id-1].getconnection()) +
				" broken situation: " + to_string(tanks[id-1].getBrokenStatus()));
		}
	}
	return "there is no such a tank with id " + to_string(id);
}
string Simulation::fill_tank(int id, double quantity)
{
	for (auto i = tanks.begin(); i != tanks.end(); ++i)
	{
		if ((*i).getfuel_tank_id() == id)
		{	
			double emptyside = (*i).getCapacity() - (*i).getFuelQuantity();
			if (emptyside < quantity) {
				(*i).setFuelQuantity((*i).getCapacity());
				return "tank " + to_string(id) + " is filled with " + to_string(emptyside) + " liter fuel because of tank capacity.";
			}
			else{
				(*i).setFuelQuantity(quantity);
				return "tank " + to_string(id) + " is filled with " + to_string(quantity) + " liter fuel.";
			}
		}
	}
	return "there is no such a tank with id " + to_string(id);
}
string Simulation::give_back_fuel(double L)
{
	vector<FuelTank> ConnectedTanks;
	for (auto i = tanks.begin(); i != tanks.end(); ++i)
	{
		if ((*i).getconnection() == true) ConnectedTanks.push_back(*i);
	}
	if (ConnectedTanks.size() == 0) return "There is no external connected tank to give fuel back.";
	vector<FuelTank> returnedTanks;
	stringstream ss;
	bool statement = false;
	while (engine->getInternalTankQuantity() > 0)
	{
		statement = true;
		double min = ConnectedTanks.at(0).getCapacity();
		int minTank_ID = 0;
		for (auto i = ConnectedTanks.begin(); i != ConnectedTanks.end(); ++i)
		{
			//daha �nce yak�t g�nderilip fullenen tank m� de�il mi? / flag=1(returnedTank)
			int flag = 0;
			for (auto j = returnedTanks.begin(); j != returnedTanks.end(); ++j)
			{
				if ((*i).getfuel_tank_id() == (*j).getfuel_tank_id())
				{
					flag == 1;
					break;
				}
			}
			//daha �nce yak�t g�nderilmemi� ve kapasitesi g�nderilmeyenlerden en k���k olan� belirleme
			if ((*i).getCapacity() < min && flag == 0)
			{
				min = (*i).getCapacity();
				minTank_ID = (*i).getfuel_tank_id();
			}
		}

		string s = to_string(minTank_ID) + " ";
		ss << s;

		double freeVolume = ConnectedTanks.at(minTank_ID).getCapacity() - ConnectedTanks.at(minTank_ID).getFuelQuantity();
		double old_fuel_quantity = ConnectedTanks.at(minTank_ID).getFuelQuantity();
		if (freeVolume >= L)
		{
			ConnectedTanks.at(minTank_ID).setFuelQuantity(old_fuel_quantity + L);
			engine->setInternalTankQuantity(0);
			break;
		}
		else
		{
			ConnectedTanks.at(minTank_ID).setFuelQuantity(old_fuel_quantity + freeVolume);
			engine->setInternalTankQuantity(L - freeVolume);
			L -= freeVolume;
		}
	}
	string returnedTankIDs = ss.str();
	if (statement == true)
		return to_string(L) + " liter fuel in internal tank is gived back tank/tanks " + returnedTankIDs;
	else
		return "There isn't fuel in internal tank.";
}
string Simulation::start_engine()
{
	///check if there is any fuel in the internal tank
	if (AbsorbControl() == false)	return "There is no connected external tank to start!";
	
	if (engine->getInternalTankQuantity() > 0 && engine->getStatus() == false)
	{
		engine->setStatus(true);
		return "Engine started!";
	}
	else if (engine->getInternalTankQuantity() == 0)
	{
		engine->setStatus(false);
		return "There is no connected external tank to start!";
	}
	else if (engine->getStatus() == true)
		return "Engine has already started!";
}
string Simulation::close_valve(int id)
{
	for (auto it = tanks.begin(); it != tanks.end(); ++it)
	{
		int idtemp = (*it).getfuel_tank_id();
		if (idtemp == id)
		{
			///check if the valve is open
			if ((*it).getValveStatus() == true)
			{
				(*it).setValveStatus(false);
				for (auto iter = ValveOpenTanks.begin(); iter != ValveOpenTanks.end(); ++iter)
				{
					if ((*iter).getfuel_tank_id() == (*it).getfuel_tank_id())
					{
						ValveOpenTanks.erase(iter);
						break;
					}
				}
				return "The valve is closed!";
			}
			else
				return "The valve is already closed!";
		}
	}
}
string Simulation::break_fuel_tank(int id)
{
	for (auto it = tanks.begin(); it != tanks.end(); ++it)
	{
		int idtemp = (*it).getfuel_tank_id();
		if (idtemp == id)
		{
			///check if the tank is broken or not
			if ((*it).getBrokenStatus() == false)
			{
				///the tank is broken now
				(*it).setBrokenStatus(true);
				return "The tank is broken!";
			}
			else
				return "The tank is already broken!";
		}
	}
}
string Simulation::repair_fuel_tank(int id)
{
	for (auto it = tanks.begin(); it != tanks.end(); ++it)
	{
		int idtemp = (*it).getfuel_tank_id();
		if (idtemp == id)
		{
			///check if the tank is broken or not
			if ((*it).getBrokenStatus() == true)
			{
				///the tank is repaired now
				(*it).setBrokenStatus(false);
				return "The tank is repaired!";
			}
			else
				return "The tank is not broken!";
		}
	}
}
vector<string> Simulation::SimulationStop()
{
	vector<string> simulationStop;
	string temp1 = "", temp = "";
	temp1 = "Engine: Simulation stopped.\n";
	simulationStop.push_back(temp1);
	for (auto i = tanks.begin(); i != tanks.end(); ++i)
	{

		if ((*i).getconnection() == true)
		{
			(*i).setconnection(false);
			temp = "Tank " + to_string((*i).getfuel_tank_id()) + ": Simulation stopped.\n" + "Valve " + to_string((*i).getfuel_tank_id()) + ": Simulation stopped.\n";
			simulationStop.push_back(temp);
		}
	}
	if (simulationStop.size() == 0) cout << "There are no connected tank and valves." << endl;
	return simulationStop;
}
bool Simulation::getStatusE()
{
	return engine->getStatus();
}