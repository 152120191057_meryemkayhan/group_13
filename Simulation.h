#pragma once
#include <vector>
#include "Engine.h"
#include "FuelTank.h"
#include <string>
using namespace std;

class Simulation
{
	int fuel_tank_count;
	vector<FuelTank> tanks;
	vector<FuelTank> ConnectedTanks;
	vector<FuelTank> ValveOpenTanks;
	Engine *engine=engine->getInstance();
	double Time;
public:
	Simulation();
	string connect_fuel_tank_to_engine(int id);
	string disconnect_fuel_tank_from_engine(int id);
	vector<string> list_connected_tanks();
	string fuelConsumption(double);
	string stop_engine();
	string open_valve(int);
	bool AbsorbControl();
	~Simulation();
	string add_fuel_tank(int capacity);
	vector<string> list_fuel_tanks() const;
	string print_fuel_tank_count() const;
	string remove_fuel_tank(int id);
	double print_total_fuel_quantity();
	double print_consumed_fuel_quantity();
	void set_time(double);
	string print_tank_info(int);
	string fill_tank(int id, double quantity);
	string give_back_fuel(double);
	string start_engine();
	string close_valve(int);
	string break_fuel_tank(int);
	string repair_fuel_tank(int);
	vector<string> SimulationStop();
	bool getStatusE();
};

