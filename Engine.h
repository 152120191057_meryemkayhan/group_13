//#pragma once
#ifndef Engine_H
#define Engine_H
using namespace std;
/**
 *\brief Attributes belonging to engine class are kept in engine class. It is the class where the FuelTank class will be used with the additions to be made later.
 */
class Engine
{
	//!@param fuel_per_second is the double variable that holds the instantaneous fuel consumption of the engine
	const double fuel_per_second = 5.5;
	//!@param status Keeps status engine running or not
	bool status;
	//!@param initialTank Main tank connected to the engine
	double internalTank;
	const double internal_tank_capacity = 55;
	//!\brief *instance for singleton
	static Engine *instance;
	//!\brief Engine() is the default constructor
	Engine()
	{
		setStatus(false);
		setInternalTankQuantity(0);
	}
public:
	//!\brief Engine *getInstance() to create only one engine
	static Engine *getInstance()
	{
		if (!instance)
			instance = new Engine();
		return instance;
	}
	//!\brief Engine() is the constructor with parameters 
	Engine(double, double, bool);
	~Engine();
	//!\brief setInternalTankQuantity to set the quantity of fuel the internal tank has
	void setInternalTankQuantity(double);
	//!\brief getInternalTankQuantity to get the quantity of fuel the internal tank has
	double getInternalTankQuantity() const;
	//!\brief get_internal_tank_capacity to get the get_internal_tank_capacity information
	double get_internal_tank_capacity() const;
	//!\brief getFuelPerSecond to get the fuel_per_second information
	double getFuelPerSecond() const;
	//!\brief getStatus to get the status information
	bool getStatus() const;
	//!\brief setStatus function to assign the value to the status variable
	void setStatus(bool);
	//!\brief setFuelPerSecond function to assign the value to the fuel_per_sec variable
};
#endif
