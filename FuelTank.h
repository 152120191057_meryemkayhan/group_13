//#pragma once
#ifndef FuelTank_H
#define FuelTank_H
using namespace std;

class FuelTank
{
	double capacity;
	double fuel_quantity;
	bool broken;
	bool valve;
	bool connection;
	int fuel_tank_id;
public:
	FuelTank();
	FuelTank(double, int);
	~FuelTank();
	void setconnection(bool);
	bool getconnection() const;
	void setfuel_tank_id(int id);
	void setCapacity(double);
	void setFuelQuantity(double);
	//!\brief setBrokenStatus to set the status of the tank. Set true if the tank is broken.
	void setBrokenStatus(bool);
	//!\brief setValveStatus to set the status of the valve. Set true if the valve is open.
	void setValveStatus(bool);
	//!\brief getBrokenStatus to get the status of the tank. Returns true if the tank is broken.
	bool getBrokenStatus() const;
	double getCapacity() const;
	double getFuelQuantity() const;
	//!\brief getValveStatus to get the status of the valve. Returns true if the valve is open.
	bool getValveStatus() const;
	int getfuel_tank_id() const;
};
#endif