#include <iostream>
#include <string>
#include <fstream>
#include "Read.h"

using namespace std;

Read::Read(int argc, char** argv) :outputFileName("Output.txt") {
	if (argc <= 1) { // argv[0] always represents the name of the executable file
		cout << "No file name entered." << endl;
		system("pause");
	}
	else if (argc == 2) inputFileName = argv[1];
	else if (argc == 3)
	{
		inputFileName = argv[1];
		outputFileName = argv[2];
	}
}
Read::~Read() {}

string Read::getInputFileName() { return inputFileName; }
string Read::getOutputFileName() { return outputFileName; }

void Read::ReadtoWrite() {
	inputFile.open(inputFileName, ios::in);
	outputFile.open(outputFileName, ios::out);
	outputFile.close();
	if (inputFile.is_open()) { // input file is open to be read
		string line;
		//It takes input string from input file until ";". 
		while (getline(inputFile, line, ';')) {
			outputFile.open(outputFileName, ios::out | ios::app);
			outputFile << line << "\n";
			outputFile.close();
			Application(line);
		}
	}
	else { //input file cannot be opened
		cout << "Failed to open Input file.." << endl;
		system("pause");
	}
	inputFile.close();
}
void Read::Write(string output) {
	outputFile.open(outputFileName, ios::out | ios::app);
	if (outputFile.is_open()) { outputFile << output << "\n"; }
	else { //input file cannot be opened
		cout << "Failed to open output file.." << endl;
		system("pause");
	}
	outputFile.close();
}
void Read::Write(vector<string> output) {
	outputFile.open(outputFileName, ios::out | ios::app);
	if (outputFile.is_open()) {
		for (auto it = output.begin(); it != output.end(); ++it)
			outputFile << (*it) << "\n";
	}
	else { //input file cannot be opened
		cout << "Failed to open Output file.." << endl;
		system("pause");
	}
	outputFile.close();
}
void Read::Write(double output) {
	outputFile.open(outputFileName, ios::out | ios::app);
	if (outputFile.is_open()) { outputFile << output << "\n"; }
	else { //input file cannot be opened
		cout << "Failed to open output file.." << endl;
		system("pause");
	}
	outputFile.close();
}
void Read::Application(string name) {
	string str1, str2, str3;
	int counter = 0;
	//Seperating strings to string and double.
	for (int i = 0; i < name.size(); i++) {
		if (name[i] == '\n') { name = name.substr(1, name.size() - 1); }
		if (name[i] == ' ' && counter == 0) {
			str1 = name.substr(0, i);
			str2 = name.substr(i + 1, name.size() - (i + 1));
			counter++;
		}
	}
	counter = 0;
	if (name == "start_engine") {
		Write(s.start_engine());
		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}
	}
	else if (name == "stop_engine") {
		Write(s.stop_engine());
		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}
	}
	else if (str1 == "give_back_fuel") {
		int quantity = stoi(str2);
		Write(s.give_back_fuel(quantity));
		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}

	}
	else if (str1 == "add_fuel_tank") {
		int id = stoi(str2);
		Write(s.add_fuel_tank(id));

		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}
	}
	else if (name == "list_fuel_tanks") {
		Write(s.list_fuel_tanks());
		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}
	}
	else if (name == "print_fuel_tank_count") {
		Write(s.print_fuel_tank_count());

		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}
	}
	else if (str1 == "remove_fuel_tank") {
		int id = stoi(str2);
		Write(s.remove_fuel_tank(id));

		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}
	}
	else if (str1 == "connect_fuel_tank_to_engine") {
		int id = stoi(str2);
		Write(s.connect_fuel_tank_to_engine(id));

		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}
	}
	else if (str1 == "disconnect_fuel_tank_from_engine") {
		int id = stoi(str2);
		Write(s.disconnect_fuel_tank_from_engine(id));

		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}
	}
	else if (name == "list_connected_tanks") {
		Write(s.list_connected_tanks());

		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}

	}
	else if (name == "print_total_fuel_quantity") {
		Write(s.print_total_fuel_quantity());
		if (s.getStatusE() == true) {
			s.fuelConsumption(1);
		}

	}
	else if (name == "print_total_consumed_fuel_quantity") {
		Write(s.print_consumed_fuel_quantity());

		if (s.getStatusE() == true) s.fuelConsumption(1);


	}
	else if (str1 == "print_tank_info") {
		int id = stoi(str2);
		Write(s.print_tank_info(id));

		if (s.getStatusE() == true) s.fuelConsumption(1);

	}
	else if (str1 == "fill_tank") {
		for (int i = 0; i < str2.size(); i++) {
			if (str2[i] == ' ') {
				string temp = str2;
				str2 = temp.substr(0, i);
				str3 = temp.substr(i + 1, name.size() - (i + 1));
			}
		}
		int id = stoi(str2);
		int quantity = stoi(str3);
		Write(s.fill_tank(id, quantity));

		if (s.getStatusE() == true) s.fuelConsumption(1);

	}
	else if (str1 == "open_valve") {
		int id = stoi(str2);
		Write(s.open_valve(id));
		if (s.getStatusE() == true) s.fuelConsumption(1);

	}
	else if (str1 == "close_valve") {
		int id = stoi(str2);
		Write(s.close_valve(id));
		if (s.getStatusE() == true) s.fuelConsumption(1);

	}

	else if (str1 == "break_fuel_tank") {
		int id = stoi(str2);
		Write(s.break_fuel_tank(id));
		if (s.getStatusE() == true) s.fuelConsumption(1);
	}

	else if (str1 == "repair_fuel_tank") {
		int id = stoi(str2);
		Write(s.repair_fuel_tank(id));
		if (s.getStatusE() == true) s.fuelConsumption(1);
	}

	else if (str1 == "wait") {
		int seconds = stoi(str2);
		Write(s.fuelConsumption(seconds));
	}

	else if (name == "stop_simulation") {
		s.fuelConsumption(1);
		Write(s.SimulationStop());
		Write("Simulation has stopped.");
		exit(0);
	}
}