#include "Engine.h"
using namespace std;
//!\param fuel double fuel parameter
//!\param status bool status parameter
Engine::Engine(double fuel, double internaltank, bool status)
{
	setStatus(status);
	setInternalTankQuantity(internaltank);
}
Engine::~Engine() {}
//!\brief getFuelPerSecond to get the fuel_per_second information
//!\return fuel_per_second
double Engine::getFuelPerSecond() const { return this->fuel_per_second; }
void Engine::setInternalTankQuantity(double internaltank) { this->internalTank = internaltank; }
double Engine::getInternalTankQuantity() const { return this->internalTank; }
double Engine::get_internal_tank_capacity() const { return this->internal_tank_capacity; }
//!\brief getStatus to get the status information
//!\return status
bool Engine::getStatus() const { return this->status; }
//!\brief setStatus function to assign the value to the status variable
//!\param status bool status parameter
void Engine::setStatus(bool status) { this->status = status; }