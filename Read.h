#ifndef READ_H
#define READ_H
#include "Simulation.h"
#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

class Read {
private:
	//!@param inputFileName is the name of inputFile to search and open the file from folder.
	string inputFileName;
	//!@param outputFileName is the name of outputFile to create the outputFile in the folder.
	string outputFileName;
	//!@param s is the main object for code management. 
	Simulation s;
	//!@param inputFile is our input file for taking input in folder it should be created before running.
	ifstream inputFile;
	//!@param inputFile is our input file for taking input in folder it should be created before running.
	ofstream outputFile;
public:
	//!\brief Read() is the constructor of this class.It sets inputFileName and outputFileName.
	Read(int argc, char**argv);
	~Read();
	//!\brief getInputFileName() returns inputFileName if necessary.
	string getInputFileName();
	//!\brief getOutputFileName() returns OutputFileName if necessary.
	string getOutputFileName();
	//!\brief ReadtoWrite() open files with their names for input and output.
	void ReadtoWrite();
	//!\brief Write(string) writes outputs to OutputFile as string.
	void Write(string);
	//!\brief Write(vector<string>) writes outputs to OutputFile as vector of string.
	void Write(vector<string>);
	//!\brief Write(double) writes outputs to OutputFile as double.
	void Write(double);
	//!\brief Application(string) checks string inputs for functions from inputFile.If it corresponds to statement it executes the functions and if it is necassary,sends parameter to functions.
	void Application(string);
};

#endif