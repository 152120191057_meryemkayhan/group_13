#include "FuelTank.h"
using namespace std;

//!\brief Default constructor. Sets false to status variable, false to valve status, 0 to capacity variable and 0 to fuel_quantity variable as initial values
FuelTank::FuelTank()
{
	setCapacity(0);
	setFuelQuantity(0);
	setBrokenStatus(false);
	setValveStatus(false);
	setfuel_tank_id(1);
	setconnection(false);
}
//!\brief Constructor with parameters. Assigns the values it takes as parameters.
FuelTank::FuelTank(double capacity, int fuel_tank_id)
{
	setCapacity(capacity);
	setFuelQuantity(0);
	setBrokenStatus(false);
	setValveStatus(false);
	setfuel_tank_id(fuel_tank_id);
	setconnection(false);
}
FuelTank::~FuelTank() {}
void FuelTank::setconnection(bool flag) { this->connection = flag; }
void FuelTank::setfuel_tank_id(int id) { this->fuel_tank_id = id; }
void FuelTank::setCapacity(double capacity) { this->capacity = capacity; }
void FuelTank::setFuelQuantity(double fuel_quantity) { this->fuel_quantity = fuel_quantity; }
void FuelTank::setBrokenStatus(bool broken)	{ this->broken = broken; }
void FuelTank::setValveStatus(bool valve) { this->valve = valve; }
double FuelTank::getCapacity() const{ return this->capacity; }
double FuelTank::getFuelQuantity() const{ return this->fuel_quantity; }
bool FuelTank::getBrokenStatus() const{ return this->broken; }
bool FuelTank::getValveStatus() const{ return this->valve; }
int FuelTank::getfuel_tank_id() const{ return this->fuel_tank_id; }
bool FuelTank::getconnection() const{ return this->connection; }
